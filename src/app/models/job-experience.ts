export class JobExperience {
    constructor(public jobTitle: string, public employer: string,
    public location: string, public startMonth: string, public startYear: string
    , public endMonth: string, public endYear: string, public stillInRole: boolean) {}
}
