export interface Cvtemplate {
    id: number;
    templateName: string;
    templateImageFile: string;
    templateHtmlFile: string;
}
