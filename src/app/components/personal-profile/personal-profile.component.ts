import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { PersonalProfile } from '../../models/personal-profile';
import { SharedGeneratingLevelService } from '../../services/shared-generator-level/shared-generating-level.service';
import { HttpProviderService } from '../../services/http-provider-service/http-provider.service';

@Component({
  selector: 'workstar-personal-profile',
  templateUrl: './personal-profile.component.html',
  styleUrls: ['./personal-profile.component.scss']
})
export class PersonalProfileComponent implements OnInit {

  selectedTemplateId: number;
  profile: PersonalProfile;
  ErrorInSave: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private generatorLevelService: SharedGeneratingLevelService,
    private httpProvider: HttpProviderService) {
    this.profile = new PersonalProfile('', '', '', '', '');
    this.ErrorInSave = false;
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.selectedTemplateId = params['id'];
    });
  }

  SavePersonalProfile() {
    let cvID = -1;
    this.httpProvider.SavePersonalProfile(this.profile, this.selectedTemplateId)
      .subscribe(res => {
        cvID = res;
        if (cvID !== -1) {
          this.generatorLevelService.setGeneratorLevel(2);
          this.router.navigateByUrl('Education/' + cvID);
        } else {
          this.ErrorInSave = true;
        } 
      },
      err => { this.ErrorInSave = true; }
      );
  }

  ReturnToTemplates() {
    this.generatorLevelService.setGeneratorLevel(0);
    this.router.navigate(['CVtemplate']);
  }
}
