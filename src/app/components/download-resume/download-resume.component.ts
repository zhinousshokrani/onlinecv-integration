import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { HttpProviderService } from '../../services/http-provider-service/http-provider.service';

@Component({
  selector: 'workstar-download-resume',
  templateUrl: './download-resume.component.html',
  styleUrls: ['./download-resume.component.scss']
})
export class DownloadResumeComponent implements OnInit {

  CVId: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private httpProvider: HttpProviderService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.CVId = params['id'];
    });
  }

  DownloadCV() {

    this.httpProvider.DownloadResume(this.CVId)
      .subscribe(
          (res) => { 
            const blob = new Blob([res], {type: 'application/octet-stream'});
            const link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "CV.docx";
            link.click();    
          } ,
      err => {
        console.log(err);
      }
      );
  }

}
