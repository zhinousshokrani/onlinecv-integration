import { Component, OnInit } from '@angular/core';
import { SwiperComponent, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Cvtemplate } from '../../models/cvtemplate';
import { SharedGeneratingLevelService } from '../../services/shared-generator-level/shared-generating-level.service';
import { HttpProviderService } from '../../services/http-provider-service/http-provider.service';
import { Router } from '@angular/router';
import { error } from 'selenium-webdriver';

@Component({
  selector: 'workstar-template-selector',
  templateUrl: './template-selector.component.html',
  styleUrls: ['./template-selector.component.scss']
})
export class TemplateSelectorComponent implements OnInit {

  templateArray: Cvtemplate[];

  public config: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false
  };


  constructor(private router: Router,
    private httpProvider: HttpProviderService,
    private generatorLevelservice: SharedGeneratingLevelService) { }

  ngOnInit() {
    this.httpProvider.getTemplates().subscribe(data => {
      this.templateArray = data;
      this.templateArray.forEach(t => {
        t.templateImageFile = '../../assets/templates/' + t.templateImageFile;
      });
    },
      err => {
        console.log('error in fetching templates from server');
        console.log();
      });
  }

  public chooseTemplate(id: number) {
    this.router.navigateByUrl('PersonalProfile/' + id);
    this.generatorLevelservice.setGeneratorLevel(1);
  }

}
