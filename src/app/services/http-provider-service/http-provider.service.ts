import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PersonalProfile } from '../../models/personal-profile';
import { Cvtemplate } from '../../models/cvtemplate';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { error } from 'selenium-webdriver';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/of';
import { Education } from '../../models/education';
import { JobExperience } from '../../models/job-experience';
import { Skill } from '../../models/skill';

@Injectable()
export class HttpProviderService {

  baseUrl: string;
  profileUrl: string;
  templateUrl: string;
  educationUrl: string;
  experienceUrl: string;
  skillUrl: string;
  resumeUrl: string;
  
  constructor(private http: HttpClient) {
    this.baseUrl = '/api';
    this.profileUrl = this.baseUrl + '/profile';
    this.educationUrl = this.baseUrl + '/education';
    this.experienceUrl = this.baseUrl + '/experience';
    this.skillUrl = this.baseUrl + '/skill';
    this.templateUrl = this.baseUrl + '/template';
    this.resumeUrl = this.baseUrl + '/resume';
  }

  public getTemplates(): Observable<Cvtemplate[]> {
    return this.http.get<Cvtemplate[]>(this.templateUrl).map(res => res);
  }

  public SavePersonalProfile(personalProfile: PersonalProfile, templateId: number): Observable<number> {
    const ProfileAndTemplate = {
      "profile": personalProfile,
      "templateId": templateId
    };

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');

    return this.http.post<number>(this.profileUrl, ProfileAndTemplate, {
      headers: headers,
    });
  }

  public getEducationByCVId(cvId: number): Observable<Education[]> {
    return this.http.get<Education[]>(this.educationUrl + "/" + cvId).map(res => res);
  }

  public SaveEducationHistory(educationHistory: Education[], cvId: number): Observable<void> {
    const educations = {
      "educationList": educationHistory,
      "cvId": cvId
    };

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');

    return this.http.post<void>(this.educationUrl, educations, {
      headers: headers,
    });
  }

  public getJobExperienceByCVId(cvId: number): Observable<JobExperience[]> {
    return this.http.get<JobExperience[]>(this.experienceUrl + "/" + cvId).map(res => res);
  }

  public SaveWorkHistory(workHistory: JobExperience[], cvId: number): Observable<void> {
    const jobs = {
      "jobList": workHistory,
      "cvId": cvId
    };

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');

    return this.http.post<void>(this.experienceUrl, jobs, {
      headers: headers,
    });
  }

  public getSkillsByCVId(cvId: number): Observable<Skill[]> {
    return this.http.get<Skill[]>(this.skillUrl + "/" + cvId).map(res => res);
  }

  public SaveSkillHistory(skillHistory: Skill[], cvId: number): Observable<void> {
    const jobs = {
      "skillList": skillHistory,
      "cvId": cvId
    };

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');

    return this.http.post<void>(this.skillUrl, jobs, {
      headers: headers,
    });
  }

  public DownloadResume(cvId: number): Observable<Blob> {
    return this.http.get(`${this.resumeUrl}/${cvId}`, { responseType: "blob"});
  }

}
