using System.Collections.Generic;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.BusinessLayer.Interfaces
{
    public interface ITemplateService
    {
        List<CVTemplate> GetTemplates();
    }
}