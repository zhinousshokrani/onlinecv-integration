using OnlineCVAPI.Models;

namespace OnlineCVAPI.BusinessLayer.Interfaces
{
    public interface ICVService
    {
        int SaveCV(int profileId, int templateId);
    }
}