using System.Collections.Generic;
using System.Linq;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.DataAccessLayer;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.BusinessLayer
{
    public class TemplateService : ITemplateService
    {
         IGenericRepository<CVTemplate> _repository;
        public TemplateService(IGenericRepository<CVTemplate> repository)
        {
            _repository=repository;
        }

        public List<CVTemplate> GetTemplates() {
            return _repository.GetAll().ToList();
        }
    }
}