using System;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.DataAccessLayer;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.BusinessLayer
{
    public class CVService : ICVService
    {
        IGenericRepository<CV> _repository;
        public CVService(IGenericRepository<CV> repository)
        {
            _repository = repository;
        }

        public int SaveCV(int profileId, int templateId)
        {
            if (profileId > 0 && templateId > 0)
            {
                CV cv = new CV();
                cv.profileId = profileId;
                cv.templateId = templateId;
                cv.creationDateTime = DateTime.Now.ToString("yyyy/MM/dd - hh:mm");
                return _repository.Create(cv);
            }
            else
                return -1;
        }
    }
}