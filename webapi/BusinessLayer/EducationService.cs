using System.Collections.Generic;
using System.Linq;
using OnlineCVAPI.BusinessLayer.Interfaces;
using OnlineCVAPI.DataAccessLayer;
using OnlineCVAPI.Models;
using OnlineCVAPI.ViewModels;

namespace OnlineCVAPI.BusinessLayer
{
    public class EducationService : IEducationService
    {
        IGenericRepository<Education> _repository;
        public EducationService(IGenericRepository<Education> repository)
        {
            _repository = repository;
        }

        public List<EducationWrapper> GetByCVId(int cvId)
        {
            return (from e in _repository.GetAll()
                    where e.CVId == cvId
                    select new EducationWrapper
                    {
                        title = e.title,
                        institute = e.institute,
                        graduationYear = e.graduationYear
                    }).ToList();
        }

        public void SaveEducationHistory(EducationHistory educationHistory)
        {
            foreach (Education edu in educationHistory.educationList)
                edu.CVId = educationHistory.cvId;

            //check if this cv id has previously education history
            var previousEducationList = _repository.GetAll().Where(x => x.CVId == educationHistory.cvId).ToArray();
            int count = _repository.GetAll().Where(x => x.CVId == educationHistory.cvId).Count();
            if (count > 0)
                _repository.DeleteRange(previousEducationList);

            _repository.CreateRange(educationHistory.educationList);
        }
    }
}