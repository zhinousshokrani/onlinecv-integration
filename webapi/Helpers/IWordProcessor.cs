using System.Collections.Generic;
using System.IO;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.Helpers
{
    public interface IWordProcessor
    {
        FileStream ReplaceWordTemplate(string contentRootPath
            , string templateFileName
            , PersonalProfile profile
            , List<Education> educationHitstory
            , List<Experience> workHistory
            , List<Skill> skills);
    }
}