using System.Collections.Generic;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.ViewModels
{
    public class EducationHistory
    {
        public List<Education> educationList {get; set;}
        public int cvId {get; set;}
    }
}