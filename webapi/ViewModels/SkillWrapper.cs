namespace OnlineCVAPI.ViewModels
{
    public class SkillWrapper
    {
        public string skillName {get; set;}
        public string institute {get; set;}
        public string year {get; set;}
    }
}