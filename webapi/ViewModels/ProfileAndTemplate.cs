using OnlineCVAPI.Models;

namespace OnlineCVAPI.ViewModels
{
    public class ProfileAndTemplate
    {
        public PersonalProfile profile {get; set;}
        public int templateId {get; set;}
    }
}