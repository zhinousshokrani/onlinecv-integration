using System.Collections.Generic;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.ViewModels
{
    public class SkillHistory
    {
        public List<Skill> skillList {get; set;}
        public int cvId {get; set;}
    }
}