using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using OnlineCVAPI.Models;

namespace OnlineCVAPI.DataAccessLayer
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<OnlineCVDBContext>();
            context.Database.EnsureCreated();
            
            if (!context.CVTemplates.Any())
            {
                context.CVTemplates.Add(new CVTemplate()
                {
                    templateName = "sample1",
                    templateDocFile = "template1.docx",
                    templateImageFile = "template1.jpg"
                });

                context.CVTemplates.Add(new CVTemplate()
                {
                    templateName = "sample2",
                    templateDocFile = "template2.docx",
                    templateImageFile = "template2.jpg"
                });

                context.CVTemplates.Add(new CVTemplate()
                {
                    templateName = "sample3",
                    templateDocFile = "template3.docx",
                    templateImageFile = "template3.jpg"
                });

                context.SaveChanges();
            }
        }
    }
}